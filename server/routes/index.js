const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');

module.exports = (app) => {
    app.use('/api/users', userRoutes);
    app.use('/api/auth', authRoutes);
    // app.use('/api/messages', userRoutes);

    // app.use('/api/fighters', fighterRoutes);
    // app.use('/api/fights', fightRoutes);
  };