const { users } = require('../models/user');
const UserService = require('../services/userService');

let ValidateFields = (ArrToValidate, model) => {
    let exist = true
    model.forEach(element => {
        if (exist && !ArrToValidate.includes(element)){
            exist = false;
            return exist
        }
    });
    return exist
}

let deleteFromArr = (arr, value) => {
    return arr.filter(item => item !== value)
}

let deleteExternalFields = (objToDel, model) =>{
    let objToDelKeys = Object.keys(objToDel);
    let modelKeys = Object.keys(model);
    objToDelKeys.forEach(element => {
        !modelKeys.includes(element) ? delete objToDel[`${element}`] : 0;
    });
    return objToDel
}

let checkForSameKeyValue = (ObjToCheck, key, base) =>{
    let answer = false;
    base.forEach(element => {
        element[key] == ObjToCheck[key] && !answer ? answer = true : false;
    });
    return answer
}

let checkForSameId = (key, value, base) =>{
    let answer = false;
    base.forEach(element => {
        String(element[key]) == String(value) && !answer ? answer = true : false;
    });
    return answer
}

let getObjectByKey = (key, value, base) =>{
    let userObj = {}
    base.forEach(element => {
        element[key] == value ? userObj = {...element} : 0;
    });
    return userObj
}

const createUserValid = (req, res, next) => {
    let inputObj = req.body;
    let userKeys = Object.keys(users);
    userKeys = deleteFromArr(userKeys, 'userId');
    let inputObjKeys = Object.keys(inputObj);
    let email = inputObj.email;
    let phoneNumber = inputObj.phoneNumber;
    let base = UserService.allUsers();

    if (!Object.keys(inputObj).length){
        req.body = [400, 'Request with empty data'];
        next();
    } else if (inputObjKeys.includes('userId')){
        req.body = [400, 'Request does not have to exist userId field'];
        next();
    } else if (!ValidateFields(inputObjKeys, userKeys)){
        req.body = [400, 'Missing some fields'];
        next();
    } else if (checkForSameKeyValue(inputObj, 'user', base)){
        req.body = [400, 'User with the same username already exist'];
        next();
    } else if (inputObj['password'].length<6){
        req.body = [400, 'Incorrect password. Enter more than 6 symbols'];
        next();
    } else {
        req.body = deleteExternalFields(inputObj, users)
        next();
    }
    //     // TODO: Implement validatior for user entity during creation
}

const updateUserValid = (req, res, next) => {
    console.log(req.body);
    let userId = req.params["id"];
    let inputObj = req.body;
    let userKeys = Object.keys(users);
    userKeys = deleteFromArr(userKeys, 'userId');
    let inputObjKeys = Object.keys(inputObj);
    let email = inputObj.email;
    let phoneNumber = inputObj.phoneNumber;
    let base = UserService.allUsers();

    if (!Object.keys(inputObj).length){
        req.body = [400, 'Request with empty data'];
        next();        
    } else if (inputObjKeys.includes('userId')){
        req.body = [400, 'Request does not have to exist userId field'];
        next();
    } else if (!ValidateFields(inputObjKeys, userKeys)){
        req.body = [400, 'Missing some fields'];
        next();
    } else if (checkForSameKeyValue(inputObj, 'user', base)){
        req.body = [400, 'User with the same username already exist'];
        next();
    }else if (inputObj['password'].length<6){
        req.body = [400, 'Incorrect password. Enter more than 6 symbols'];
        next();
    } else if (!checkForSameId("userId", userId, base)){
        req.body = [404, 'User with this id is not found'];
        next();
    } else {
        req.body = deleteExternalFields(inputObj, users)
        next();
    }
}

const deleteUserById = (req, res, next) => {
    let id = req.params["id"];
    let base = UserService.allUsers();

    if (!checkForSameId("userId", id, base)){
        req.body = [404, 'User with this id is not found'];
        next();
    } else {
        let a = getObjectByKey("userId", req.params["id"], base)
        console.log("deleteUserById -> a", a)
        req.body = getObjectByKey("userId", req.params["id"], base)
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.deleteUserById = deleteUserById;
