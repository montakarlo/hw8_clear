import {LOG_IN,
  SHOW_LOADER,
  HIDE_LOADER,
  FETCH_MESSAGES,
  FETCH_USERS,
  DELETE_USER} from '../actions/actionTypes';

const initialState = {
  fetchedUsers: []
}

export default function fetchUsers (state = initialState, action) {

  switch(action.type) {
    case FETCH_USERS:
      return {
        ...state, fetchedUsers: action.payload
      }
    case DELETE_USER:
      return {
      }
    default:
      return state
  }
}
