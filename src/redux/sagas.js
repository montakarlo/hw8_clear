import {takeEvery, put, call} from 'redux-saga/effects'
import {LOG_IN_REQUEST, SHOW_LOADER} from './actions/actionTypes'
import { showLoader, hideLoader } from './actions/actions'

export function* sagaWatcher(){
  yield takeEvery(LOG_IN_REQUEST, sagaWorker)
}

function* sagaWorker (){
  try {
    yield put(showLoader())
    const payload = yield call(logInRequest)
    yield put({type: LOG_IN_REQUEST, payload})
    yield put(hideLoader())
  } catch (error) {
    yield put(hideLoader())
  }
}

async function logInRequest(logInObj) {
    const response = await fetch('https://routing-chat.herokuapp.com/api/auth',{
      method: 'POST',
      headers: {'Content-Type': 'application/json;charset=utf-8'},
      body: JSON.stringify(logInObj)
    });
    return await response.json();
}