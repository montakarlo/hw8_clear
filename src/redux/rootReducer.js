import {combineReducers} from 'redux'

import messages from './reducers/messages'
import chat from './reducers/chat'
import logIn from './reducers/logIn'
import appReducer from './reducers/appReducer'
import fetchUsers from './reducers/fetchUsers'

export default combineReducers({
  messages, chat, logIn, appReducer, fetchUsers
})
