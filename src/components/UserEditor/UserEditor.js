import React, {Component} from 'react';
import './UserEditor.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'


export class UserEditor extends Component {


  goToUserList = () => {
    this.props.history.push({
      pathname: '/userList'
    })
  }
  
  render(){
    return(
      <div className = "container">
        <Pageheader text='User editor' className="pageHeader"/>
        <form action="submit" className='logInForm'>
          <div className='logInFormContainer'>

            <input
              type="text"
              id="input_userName"
              name="input_text"
              placeholder="User name"
              >
            </input>

            <input
              type="text"
              id="input_avatarLink"
              name="input_text"
              placeholder="Avatar link"
              >
            </input>

            <input
              type="text"
              id="input_password"
              name="input_text"
              placeholder="Password"
              >
            </input>

            <input
              type="text"
              id="input_passwordConfirm"
              name="input_text"
              placeholder="Confirm password"
              >
            </input>

            <button
              id = "submitButton"
              type="submit"
              value="Submit"
              className="logIn__button"
              onClick={this.goToUserList}
            >Ok
            </button>
          </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

export default connect(mapStateToProps, null)(UserEditor)