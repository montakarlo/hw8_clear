import React, {Component} from 'react';
import './EditMessage.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'


export class EditMessage extends Component {

  goToChat = () => {
    this.props.history.push({
      pathname: '/chat'
    })
  }

  render(){
    return(
      <div className = "container">
        <div className='pageHeaderContainer'>
          <Pageheader text='Edit message' className="pageHeader"/>
        </div>
        <form className="editArea">
            <div className="editAreaContainer">
              <textarea
                className="textareaEditArea"
                type="text"
                id="edit_message"
                name="edit_text"
                placeholder="Type message!"
                >
              </textarea>
            </div>
            <div className='editButtonContainer'>
              <button
                type="submit"
                value="Submit"
                className="editArea__button editArea__button_cancel"
                onClick={this.goToChat}
                >Cancel
              </button>
              <button
                type="submit"
                value="Submit"
                className="editArea__button"
                onClick={this.goToChat}
                >Ok
              </button>
            </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

export default connect(mapStateToProps, null)(EditMessage)
