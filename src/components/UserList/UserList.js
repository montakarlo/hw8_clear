import React, {Component} from 'react';
import './UserList.sass'
import Pageheader from '../Chat/Pageheader/Pageheader'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'
import UserItem from './UserItem/UserItem'
import {fetchUsers} from '../../redux/actions/actions'
import {Loader} from '../../components/Loader/Loader'

export class UserList extends Component {

  componentDidMount = () => {
    this.props.onFetchUsers()
  }
  goToUserEditor = () => {
    this.props.history.push({
      pathname: '/userEditor'
    })
  }
  render(){    
    if (this.props.loading){
      return <Loader />
    }
    
    return(
      <div className = "container">
        <div className='pageHeaderContainer'>
          <Pageheader text='User list' className="pageHeader"/>
          <NavLink  to='/chat'>{`<Go to chat`}</NavLink>
        </div>
        <div className='usersContainer'>
          <button
            id = "submitButton"
            type="submit"
            value="Submit"
            className="addUser__button"
            onClick={this.goToUserEditor}
          >Add user
          </button>
          <div className='usersArea'>
            <div className='usersArea__container'>
              {this.props.fetchUsers.fetchedUsers.map((user, index) => {
                return(
                  <UserItem
                  history={this.props.history}
                  user = {user.user}
                  password = {user.password}
                  avatar = {user.img}
                  userId = {user.userId}
                  key = {index}
                  />
                )
              })
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    fetchUsers: state.fetchUsers,
    loading: state.appReducer.loading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchUsers: () => dispatch(fetchUsers()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)