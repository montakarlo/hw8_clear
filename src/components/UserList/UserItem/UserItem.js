import React, {Component} from 'react';
import './UserItem.sass'
import {connect} from 'react-redux'
import {NavLink, Route, Switch, Redirect} from 'react-router-dom'
import {deleteUser} from '../../../redux/actions/actions'
export class UserItem extends Component {
  constructor(props){
    super(props);
  }
  goToUserEditor = () => {
    this.props.history.push({
      pathname: '/userEditor'
    })
  }
  goToUserList = () => {
    this.props.history.push({
      pathname: '/userList'
    })
  }
  render(){
    return(
      <div className='userItemContainer'>
        <div className='userName'>
          <span>{this.props.user}</span>
        </div>
        <div className='userButtonsContainer'>
          <button
            type="submit"
            value="Submit"
            className="user__button deleteUser__button"
            onClick={() => {
              this.props.onDelete(this.props.userId)
            }
            }

          >Delete
          </button>
          <button
            type="submit"
            value="Submit"
            className="user__button editUser__button"
            onClick={this.goToUserEditor}
          >Edit
          </button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loaded: state.loaded
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onDelete: id => dispatch(deleteUser(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserItem)